{-# LANGUAGE GADTs #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}

{-# OPTIONS_HADDOCK show-extensions #-}
-----------------------------------------------------------------------------
-- |
-- Module      : Cat2SBM
-- Copyright   : (c) Nachiappan Valliappan 2018
-- Maintainer  : nachivpn@gmail.com
-- Stability   : experimental
-- Portability : non-portable

-- Compiles @Mph@ morphisms to a list of @SBM@ instructions.
--
-----------------------------------------------------------------------------

module Cat2SBM (
   mph2sbm,
   countCurry
 ) where

import Categories
import SBM
import Simplicity
import Debug.Trace

-- | Translates 'Mph' morphisms into lists of machine instructions.
-- The integer argument represents the size of pointers
mph2sbm :: Mph Types a b -> Int -> [Inst]

mph2sbm (Id :: Mph Types a b) sp = [Copy $ bsize (undefined :: a) sp]

mph2sbm ((g :: Mph Types b c) `O` (f :: Mph Types a b)) sp =
        [NewFrame $ bsize (undefined :: b) sp]
        ++ mph2sbm f sp
        ++ [MoveFrame]
        ++ mph2sbm g sp
        ++ [DropFrame]

mph2sbm Terminal _ = [Nop]

mph2sbm (Inj1 :: Mph Types a ab) sp =
        [ Write False
        , Skip $ padL  (undefined :: ab) sp
        , Copy $ bsize (undefined :: a)  sp
        ]

mph2sbm (Inj2 :: Mph Types b ab) sp =
        [ Write True
        , Skip $ padR  (undefined :: ab) sp
        , Copy $ bsize (undefined :: b)  sp
        ]

mph2sbm (Copair -- :: Mph Types ((a :+: b) :*: e) c
            (p :: Mph Types (a :*: e) c)
            (q :: Mph Types (b :*: e) c)) sp =
        [ Br ([ Fwd      $ 1 + padL (undefined :: (a :+: b)) sp
              , NewFrame $ bsize (undefined :: (a :*: e)) sp
              , Copy     $ bsize (undefined :: (a :*: e)) sp
              , MoveFrame]
             ++ mph2sbm p sp)
             ([ Fwd      $ 1 + padR (undefined :: (a :+: b)) sp
              , NewFrame $ bsize (undefined :: (b :*: e)) sp
              , Copy     $ bsize (undefined :: (b :*: e)) sp
              , MoveFrame]
             ++ mph2sbm q sp)
        , DropFrame]

mph2sbm (Factor -- :: Mph Types a (b :*: c)
                (p :: Mph Types a b)
                (q :: Mph Types a c)) sp = mph2sbm p sp ++ mph2sbm q sp

mph2sbm (Fst :: Mph Types ab a) sp = [Copy $ bsize (undefined :: a) sp]

mph2sbm (Snd :: Mph Types ab b) sp =
    [ Fwd  $ bsizeF (undefined :: ab) sp
    , Copy $ bsize  (undefined :: b)  sp
    , Bwd  $ bsizeF (undefined :: ab) sp
    ]

mph2sbm (Curry -- :: Mph Types r (a :=>: b)
               (l :: Mph Types (r :*: a) b)) sp =
    let rSize = bsize (undefined :: r) sp
    in [PutCls (mph2sbm l sp) rSize sp]

mph2sbm (Eval :: Mph Types a2bna b) sp =
    let a2bSize = sp -- bsizeF (undefined :: a2bna) sp
        aSize   = bsizeS (undefined :: a2bna) sp
    in [EvalCls a2bSize aSize]

-- | Counts the number of closures in a given morphism
countCurry :: Mph Types a b -> Int
countCurry (Curry f)    = 1 + countCurry f
countCurry (Factor f g) = countCurry f + countCurry g
countCurry (Copair f g) = countCurry f + countCurry g
countCurry (O f g)      = countCurry f + countCurry g
countCurry _            = 0
