{-# LANGUAGE GADTs #-}
{-# LANGUAGE ScopedTypeVariables,TypeOperators #-}
{-# LANGUAGE RankNTypes #-}
module Example where


import Categories
import Simplicity
import Debug.Trace
import SBM
--import Simpl2SBM
import Cat2SBM
import Simpl2Cat
import Denotational

type SBool = T :+: T

duplicate :: Simpl SBool (SBool :*: SBool)
duplicate = Pair Iden Iden

iden :: Simpl SBool SBool
iden = Iden

not' :: Simpl SBool SBool
not' = Comp (Pair Iden Unit) (Case (Injr Unit) (Injl Unit))

idenf :: (Types r, Types a) =>
    Simpl r (a :=>: a)
idenf = Lam (Drop Iden)

constf :: (Types r, Types a, Types b) =>
    Simpl r (a :=>: (b :=>: a))
constf = Lam (Lam (Take (Drop Iden)))

flipf :: (Types r, Types a, Types b, Types c) =>
    Simpl r (a :=>: (b :=>: c)) -> Simpl r b -> Simpl r a -> Simpl r c
flipf f b a = App (App f a) b

comp :: (Types r, Types a, Types b, Types c) =>
    Simpl r (a :=>: b) -> Simpl r (b :=>: c) -> Simpl r (a :=>: c)
comp f g = Lam (App (Take g) (App (Take f) (Drop Iden)))

--example1 :: (Types a, Types b) => Simpl a b -> SBM Bit
--example1 = example simpl2sbm

runSimpl :: (Types a, Types b) => Simpl a b -> IO ()
runSimpl (s :: Simpl a b) = do
        putStrLn $ "Number of curry: " ++ show c1
        putStrLn $ "Number of input exponentials: " ++ show c2
        putStrLn $ "Size of pointers: " ++ show sp
        putStrLn $ "Size of the output: " ++ show sb
        putStrLn $ show $ evalProg (mph2sbm m sp) sb
        return ()
 where c1 = countCurry m
       c2 = countExp (undefined :: a)
       sp = floor $ (log (fromIntegral (c1 + c2)) / log 2) + 1
       m  = simpl2bcc s
       sb = bsize (undefined :: b) sp

true :: Types a => Simpl a SBool
true = Injr Unit

false :: Types a => Simpl a SBool
false = Injl Unit

idapp :: Simpl SBool SBool
idapp = App idenf true

constapp :: Simpl SBool SBool
constapp = App (App constf true) false

flipapp :: Simpl SBool SBool
flipapp = flipf constf true false

initFalse :: Simpl T SBool
initFalse = false

(.>) :: (Types a, Types b, Types c) => Simpl a b -> Simpl b c -> Simpl a c
(.>) = Comp

-- # Mapping
map1 :: (Types a, Types b) => Simpl T (a :=>: b) -> Simpl a b
map1 f = App (Unit .> f) Iden

map2 :: (Types a, Types b) => Simpl T (a :=>: b) -> Simpl (a :*: a) (b :*: b)
map2 f = Pair (Take $ map1 f) (Drop $ map1 f)

map3 :: (Types a, Types b) => Simpl T (a :=>: b)
     -> Simpl (a :*: a :*: a) (b :*: b :*: b)
map3 f = Pair (Take $ map2 f) (Drop $ map1 f)

map4 :: (Types a, Types b) => Simpl T (a :=>: b)
     -> Simpl (a :*: a :*: a :*: a) (b :*: b :*: b :*: b)
map4 f = Pair (Take $ map3 f) (Drop $ map1 f)

four :: Simpl T (SBool :*: SBool :*: SBool :*: SBool)
four = Pair (Pair (Pair true false) true) false

three :: Simpl T (SBool :*: SBool :*: SBool)
three = (Pair (Pair true false) true)

notf :: Simpl T (SBool :=>: SBool)
notf = Lam $ Drop not'

initf :: Simpl T (SBool :=>: SBool)
initf = Lam $ Drop (Injr Unit)

exMap1 = four .> map4 idenf
exMap2 = four .> map4 notf
exMap3 = four .> map4 initf
exMap4 = three .> map3 initf

type SNat = forall a. Types a => Simpl (a :=>: a) (a :=>: a)

zero :: SNat
zero = Lam (Drop Iden)

one :: SNat
one = Lam (App (Take Iden) (Drop Iden))

two :: SNat
two = Lam $ App (Take Iden) (App (Take Iden) (Drop Iden))

succ' :: SNat -> SNat
succ' n = Lam $ App (App (toLam n) s) (App s z)
    where
        s = Take Iden
        z = Drop Iden

toLam :: (Types a, Types b, Types r) => Simpl a b -> Simpl r (a :=>: b)
toLam s = Lam (Drop s)

loop :: forall a. Types a => Simpl a a -> SNat -> Simpl a a
loop f n = App (App (toLam n) (toLam f)) Iden

test0 :: Simpl SBool SBool
test0 = true .> loop not' zero

test1 :: Simpl SBool SBool
test1 = true .> loop not' one

test2 :: Simpl SBool SBool
test2 = true .> loop not' two
