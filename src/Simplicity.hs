{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ScopedTypeVariables#-}
{-# LANGUAGE TypeFamilies#-}

{-# OPTIONS_HADDOCK show-extensions #-}
-----------------------------------------------------------------------------
-- |
-- Module      : Simplicity
-- Copyright   : (c) Nachiappan Valliappan 2018
-- Maintainer  : nachivpn@gmail.com
-- Stability   : experimental
-- Portability : non-portable

-- An EDSL for Simplicity programs
--
-----------------------------------------------------------------------------

module Simplicity (
  -- | An EDSL for Simplicity programs

  -- * Terms
  Simpl (..)

  -- * Type constraints
  , Types (..)
  , ProductTypes (..)
  , SumTypes (..)
  ) where

import Data.Either
import Categories ((:*:), (:+:), (:=>:), T)

-- | Simplicity terms are parametrized over an input type @i@ and an
-- output type @o@.
data Simpl i o where
    Iden :: Types a => Simpl a a
    Comp :: (Types a, Types b, Types c) => Simpl a b -> Simpl b c -> Simpl a c
    Unit :: Types a => Simpl a T
    Injl :: (Types a, Types b, Types c) => Simpl a b -> Simpl a (b :+: c)
    Injr :: (Types a, Types b, Types c) => Simpl a c -> Simpl a (b :+: c)
    Case :: (Types a, Types b, Types c, Types d) =>
            Simpl (a :*: c) d -> Simpl (b :*: c) d -> Simpl ((a :+: b) :*: c) d
    Pair :: (Types a, Types b, Types c) =>
            Simpl a b -> Simpl a c -> Simpl a (b :*: c)
    Take :: (Types a, Types b, Types c) => Simpl a c -> Simpl (a :*: b) c
    Drop :: (Types a, Types b, Types c) => Simpl b c -> Simpl (a :*: b) c
    Lam  :: (Types r, Types a, Types b) =>
            Simpl (r :*: a) b -> Simpl r (a :=>: b)
    App  :: (Types r, Types a, Types b) =>
            Simpl r (a :=>: b) -> Simpl r a -> Simpl r b

-- | The 'Types' class is used __only__ for Simplicity's types
class Types a where
  -- | Size in bits of a type @a@. It takes an extra parameter
  -- indicating the size of pointers used by the Bit Machine
  bsize :: a -> Int -> Int

  -- | Number of exponentials in a type @a@. Useful to inspect the
  -- input type of a simplicity term
  countExp :: a -> Int

-- | Type class implemented __only__ by Simplicity's products.
-- Indicates the size in bits of its first (@bsizeF@) and second
-- (@bsizeS@) components
class ProductTypes a where
  bsizeF :: a -> Int -> Int
  bsizeS :: a -> Int -> Int

-- | Type class implemented __only__ by Simplicity's coproducts.
-- Indicates the number of padded bits in the left (@padL@) and right
-- (@padR@) injections
class SumTypes a where
  padL :: a -> Int -> Int
  padR :: a -> Int -> Int

-- |  Unit is a Simplicity type
instance Types T where
    bsize _ sp = 0
    countExp _ = 0

-- | Products are Simplicity's types
instance (Types a, Types b) => Types (a :*: b) where
  bsize _ sp = let a = bsize (undefined :: a) sp
                   b = bsize (undefined :: b) sp
               in a + b

  countExp _ = let a = countExp (undefined :: a)
                   b = countExp (undefined :: b)
               in a + b

-- | Coproducts are Simplicity's types
instance (Types a, Types b) => Types (a :+: b) where
  bsize _ sp = let a = bsize (undefined :: a) sp
                   b = bsize (undefined :: b) sp
               in 1 + max a b

  countExp _ = let a = countExp (undefined :: a)
                   b = countExp (undefined :: b)
               in max a b

-- | Exponentials are Simplicity's types
instance (Types a, Types b) => Types (a :=>: b) where
  bsize _ sp = sp
  countExp _ = let b = countExp (undefined :: b)
               in 1 + b

-- | Inspect Simplicity's products
instance (Types a, Types b) => ProductTypes (a :*: b) where
  bsizeF _ sp = bsize (undefined :: a) sp
  bsizeS _ sp = bsize (undefined :: b) sp

-- | Inspect Simplicity's coproducts
instance (Types a, Types b) => SumTypes (a :+: b) where
  padL _ sp = let a = bsize (undefined :: a) sp
                  b = bsize (undefined :: b) sp
              in (max a b) - a

  padR _ sp = let a = bsize (undefined :: a) sp
                  b = bsize (undefined :: b) sp
              in (max a b) - b
