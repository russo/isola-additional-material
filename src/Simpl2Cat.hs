{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}

{-# OPTIONS_HADDOCK show-extensions #-}
-----------------------------------------------------------------------------
-- |
-- Module      : Simpl2Cat
-- Copyright   : (c) Nachiappan Valliappan 2018
-- Maintainer  : nachivpn@gmail.com
-- Stability   : experimental
-- Portability : non-portable

-- A translation from Simplicity to a Bicartesian closed category with
-- Simplicity's types as objects
--
-----------------------------------------------------------------------------

module Simpl2Cat where

import Simplicity
import Categories

-- | Translates Simplicity terms to morphisms of a Bicartesian closed
-- category ('Mph') with Simplicity types as objects
simpl2bcc :: Simpl i o -> Mph Types i o
simpl2bcc Iden          = Id
simpl2bcc (Comp f g)    = simpl2bcc g `O` simpl2bcc f
simpl2bcc Unit          = Terminal
simpl2bcc (Injl f)      = Inj1 `O` simpl2bcc f
simpl2bcc (Injr f)      = Inj2 `O` simpl2bcc f
simpl2bcc (Pair p q)    = simpl2bcc p `x` simpl2bcc q
simpl2bcc (Take f)      = simpl2bcc f `O` Fst
simpl2bcc (Drop f)      = simpl2bcc f `O` Snd
simpl2bcc (Case p q)    = Copair (simpl2bcc p) (simpl2bcc q)
simpl2bcc (Lam f)       = Curry (simpl2bcc f)
simpl2bcc (App f a)     = Eval `O` (simpl2bcc f `x` simpl2bcc a)
