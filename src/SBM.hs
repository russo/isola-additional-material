{-# LANGUAGE GADTs #-}

{-# OPTIONS_HADDOCK show-extensions #-}
-----------------------------------------------------------------------------
-- |
-- Module      : SBM
-- Copyright   : (c) Nachiappan Valliappan 2018
-- Maintainer  : nachivpn@gmail.com
-- Stability   : experimental
-- Portability : non-portable

-- Implementation of Simplicity's Bit Machine as an State monad
--
-----------------------------------------------------------------------------

module SBM (
   -- | Implementation of Simplicity's Bit Machine as an State monad

   -- * Machine's components
   Bit
   , Frame
   , Stack
   , Closure
   , ClsRef
   , Machine (..)
   , Inst (..)

   -- * The State Bit Machine
   , SBM
   , get'
   , runStep
   , run
   , evalProg

   -- * Miscellaneous
   , activeFrame
   , readMany
   ) where

import Control.Monad
import Control.Monad.State
import Control.Monad.Except
import Data.Maybe (catMaybes, fromJust)
import Debug.Trace

debugging = False

-- | A @Bit@ of data is simply a boolean
type Bit = Bool

-- | A @Frame@ is a chunk of memory with a pointer
type Frame = ([Maybe Bit],Int)

-- | A @Stack@ is a list of frames
type Stack = [Frame]

-- | A @Closure@ is a list instructions with a value representing the
-- environment
type Closure = ([Inst],[Maybe Bit])

-- | A list of closures
type ClsRef = [Closure]

-- | A @Machine@ consists a stack for reading ('readStack'), an stack
-- for writing ('writeStack'), and of a list of closures
data Machine = Machine { readStack  :: Stack
                       , writeStack :: Stack
                       , closures   :: ClsRef
                       }

-- | Instructions to manipulate the state of the Bit Machine
data Inst
    = Nop          -- ^ Does not change the state of the machine
    | Write Bit    -- ^ Writes a bit @b@ to the current read pointer
    | Copy Int     -- ^ Copies @n@ cells from active read frame to active write
                   -- frame
    | Skip Int     -- ^ Advances the write pointer by @n@ cells
    | Fwd Int      -- ^ Moves the read pointer forward by @n@ cell
    | Bwd Int      -- ^ Moves the read pointer back by @n@ cells
    | NewFrame Int -- ^ Pushes a new frame of size @n@ on the write stack
    | MoveFrame    -- ^ Moves active frame from write stack to read stack
    | DropFrame    -- ^ Drops active frame from read stack
    | Read         -- ^ Reads the value of the bit under the active read frame's
                   -- pointer
    | Br [Inst] [Inst]      -- ^ Gets the result of a 'Read' instruction, then
                            -- if the bit value is @0@ executes the first list
                            -- of instructions, else the second.
    | PutCls [Inst] Int Int -- ^ Creates a closure given a list of instructions
                            -- and the sizes of its environment and pointer
    | EvalCls Int Int       -- ^ Evaluates a closure given the sizes of its
                            -- pointer and output type
      deriving Show

-- | Stateful Bit Machine (SBM) monad
type SBM = State Machine

-- | Returns the state of the machine
get' :: SBM Machine
get' = get

-- | Performs a machine instruction
runStep :: Inst -> SBM (Maybe Bit)

runStep Nop = return Nothing

runStep (Write b) = do
  ((awf,wp),tws) <- activeFrame writeStack
  case awf !! wp of
    Nothing -> do
      let (l,r) = splitAt wp awf
          awf'  = l ++ [Just b] ++ tail r
      modify (\s -> s { writeStack  = (awf',wp + 1) : tws })
  return Nothing

runStep (Copy n) = do
  ((arf,rp),trs) <- activeFrame readStack
  ((awf,wp),tws) <- activeFrame writeStack
  let cells = (take n . drop rp) arf
      (l,r) = splitAt wp awf
      awf'  = l ++ cells ++ drop n r
  modify (\s -> s { writeStack  = (awf',wp + n) : tws })
  return Nothing

runStep (Skip n) = do
  ((awf,wp),tws) <- activeFrame writeStack
  modify (\s -> s { writeStack  = (awf,wp + n) : tws })
  return Nothing

runStep (Fwd n) = do
  ((arf,rp),trs) <- activeFrame readStack
  modify (\s -> s { readStack  = (arf,rp + n) : trs })
  return Nothing

runStep (Bwd n) = do
  ((arf,rp),trs) <- activeFrame readStack
  modify (\s -> s { readStack  = (arf,rp - n) : trs })
  return Nothing

runStep (NewFrame n) = do
  let frame  = (replicate n Nothing,0)
  ws <- writeStack <$> get
  modify (\s -> s { writeStack  = frame : ws })
  return Nothing

runStep MoveFrame = do
  ((hwf,_),tws) <- activeFrame writeStack
  rs' <- ((hwf,0) :) . readStack <$> get
  modify (\s -> s { writeStack  = tws
                  , readStack   = rs' })
  return Nothing

runStep DropFrame = do
  trs <- tail . readStack <$> get
  modify (\s -> s { readStack  = trs })
  return Nothing

runStep Read = do
  uncurry (!!) . fst <$> activeFrame readStack

runStep (Br l r) = do
  b <- runStep Read
  case b of
    Just False -> run l
    Just True  -> run r

runStep (PutCls inst rSize sp) = do
  rBits <- readMany rSize
  cls <- closures <$> get
  modify (\s -> s { closures = cls ++ [(inst,rBits)] })
  let ptr  = toBits (length cls)
  let ptr' = (map (\x -> Just x) ptr) ++ (replicate (sp - (length ptr)) Nothing)
  mapM_ (runStep . writeOrSkip) ptr'
  return Nothing

runStep (EvalCls a2bSize aSize) = do
  ptr   <- readMany a2bSize
  runStep $ Fwd a2bSize
  a     <- readMany aSize
  runStep $ Bwd a2bSize
  cls <- (closures <$> get)
  let (l,r) = cls !! (toInt ptr)
  let rnaSize = length r + aSize
  runStep $ NewFrame rnaSize
  mapM_ (runStep . writeOrSkip) (r ++ a)
  run $ [MoveFrame] ++ l ++ [DropFrame]
  return Nothing

-- | Runs a (non-empty) list of machine instructions
run :: [Inst] -> SBM (Maybe Bit)
run [i]      = runStep i
run (i : is) = runStep i >> run is

-- | Runs a (non-empty) list of machine instructions and returns its
-- active 'writeStack'
run' :: [Inst] -> SBM [Maybe Bit]
run' is = do run is
             ((hwf,_), _) <- activeFrame writeStack
             return hwf

-- | Evaluates a program (list of instructions) initializing the 'writeStack'
-- with a frame of @n@ undefined values. Returns the active 'writeStack'
evalProg :: [Inst] -> Int -> [Maybe Bit]
evalProg inst n = evalState (run' inst) initMachine
    where initMachine = Machine { readStack  = [([], 0)]
                                , writeStack = [(replicate n Nothing, 0)]
                                , closures   = []}

--------------------------------------------------------------------------------
-- Auxiliary functions

-- | Reads @n@ bits from the 'readStack'
readMany :: Int -> SBM [Maybe Bit]
readMany 0 = return []
readMany n = do
    b <- runStep Read
    runStep $ Fwd 1
    bs <- readMany $ n - 1
    runStep $ Bwd 1
    return $ b : bs

-- | Returns the active frame (topmost) from a stack
activeFrame ::
    (Machine -> Stack)     -- ^ specifies the stack to return the frame from
    -> SBM (Frame,[Frame]) -- ^ returns active frame and remaining stack
activeFrame f = (\stack -> (head stack, tail stack)) . f <$> get

-- | A more flexible version of 'Write', that will skip the cell if
-- ask to write 'Nothing'.
writeOrSkip :: Maybe Bit -> Inst
writeOrSkip Nothing  = Skip 1
writeOrSkip (Just b) = Write b

-- | If debugging is on, adds the current state of the machine to the trace
-- (read and write stack + closure), with a message @str@.
debugM :: String -> SBM (Maybe Bit)
debugM str =
 if debugging
 then get' >>= \s -> trace (str ++ show (readStack s)
                                ++ "  " ++ show (writeStack s)
                                ++ "  " ++ show (closures s))
                            (return Nothing)
 else return Nothing

toBits :: Int -> [Bit]
toBits 0 = [False]
toBits 1 = [True]
toBits n = (toBits $ n `mod` 2) ++ (toBits (n `div` 2))

toInt :: [Maybe Bit] -> Int
toInt (Just True  : bs) = 1 + 2 * toInt bs
toInt (Just False : bs) = 2 * toInt bs
toInt _                 = 0
